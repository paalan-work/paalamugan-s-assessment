# Glidewell React Code Assessment

This project represents the code assessment for a React developer looking to join Glidewell.

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

## Code Assessment

We want a simple react app with a small set of features.

### Setup

1. Clone the repo: {REPO HERE}
2. Change directory to the project directory and run “npm install” then “npm start”.

### Complete the following

- When the homepage loads, a request to an API should be made.
  - API: GET https://prolog-api.profy.dev/project
    - Returns an array of project data.
  - This data should be displayed on the homepage below the existing content for each project returned.
    - Name
    - Status
    - Language
    - Link
  - Each project should have a link to a page for that specific project
    - Link: “/project/:projectId”
    - Note: This page is not created yet, you must create this page.
  - The project page should fetch the data for the specific project by id on load:
    - API: GET https://prolog-api.profy.dev/project/{project_id}
    - Display the following data for the project:
      - Name
      - Status
      - Language
      - Number of Issues
      - Number of Events 24h
- The header has a login button. Upon clicking this button, fake log in a user and store in memory somewhere.
  - Note: Does not need to talk to an API.
  - The logged in user should display on the home page and the about page.
  - The login button in the header should become “Logout”.
    - Upon clicking logout, the user should be logged out and the button should go back to "Login".
- The footer has a logout button.
  - This should only display if the user is logged in.
  - Upon clicking logout, the user should be logged out and the button should not appear on the page.

### Commit Work

Once you have completed the above, push your code to a branch and open a merge request.
