import { useAuth } from "@/context/AuthContext";

export const useUser = () => {
  const auth = useAuth();
  return auth.user;
};
