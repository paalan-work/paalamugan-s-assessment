/* eslint-disable react-hooks/exhaustive-deps */
import { useCallback, useEffect, useRef, useState } from "react";

export default function useAsync<TData extends unknown, TDependencies extends Array<unknown>>(
  callback: (signal: AbortSignal) => Promise<TData>,
  dependencies: TDependencies[] = []
) {
  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState<Error | null>(null);
  const [data, setData] = useState<TData | null>(null);

  const callbackRef = useRef(callback);

  useEffect(() => {
    callbackRef.current = callback;
  }, [callback]);

  const callbackMemoized = useCallback(
    (signal: AbortSignal) => {
      setIsLoading(true);
      setError(null);
      setData(null);
      callbackRef
        .current(signal)
        .then((data) => {
          setData(data);
          setIsLoading(false);
        })
        .catch((err) => {
          if (err.name === "AbortError") return;
          setIsLoading(false);
          setError(err);
        });
    },
    [...dependencies]
  );

  useEffect(() => {
    const controller = new AbortController();
    const signal = controller.signal;
    callbackMemoized(signal);
    return () => controller.abort();
  }, [callbackMemoized]);

  return { isLoading, error, data };
}
