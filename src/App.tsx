import React from "react";
import "./App.scss";
import Home from "./pages/home";
import Header from "./components/header/header";
import Footer from "./components/footer/footer";
import About from "./pages/about";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import ShowSingleProject from "./pages/project/projectId";
import { AuthProvider } from "./context/AuthContext";

const App: React.FC = () => {
  return (
    <div className="App">
      <BrowserRouter>
        <AuthProvider>
          <Header />
          <Switch>
            <Route path="/" exact>
              <Home />
            </Route>
            <Route path="/about" exact>
              <About />
            </Route>
            <Route path="/project/:projectId" exact>
              <ShowSingleProject />
            </Route>
          </Switch>
          <Footer />
        </AuthProvider>
      </BrowserRouter>
    </div>
  );
};

export default App;
