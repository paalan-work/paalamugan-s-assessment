import React from "react";
import { PrologProjectList } from "@/components/PrologProjectList/index";
import { BaseLayout } from "@/layouts/BaseLayout";
import { useUser } from "@/hooks/useUser";

const Home: React.FC = () => {
  const user = useUser();
  return (
    <BaseLayout>
      <h2 className="mb-1">Home</h2>
      <p>Welcome to the home component.</p>
      <p className="mb-1">
        The currently logged in user is <strong>{user?.name}</strong>
      </p>

      <PrologProjectList />
    </BaseLayout>
  );
};

export default Home;
