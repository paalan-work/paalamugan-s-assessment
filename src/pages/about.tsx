import React from "react";
import { BaseLayout } from "@/layouts/BaseLayout";
import { useUser } from "@/hooks/useUser";

const About: React.FC = () => {
  const user = useUser();

  return (
    <BaseLayout>
      <h2>About</h2>
      <p>Welcome to the about page.</p>
      <p>
        The currently logged in user is <strong>{user?.name}</strong>
      </p>
    </BaseLayout>
  );
};

export default About;
