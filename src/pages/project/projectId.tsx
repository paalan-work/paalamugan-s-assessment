import { FC } from "react";
import useFetch from "@/hooks/useFetch";
import { IPrologProjectData } from "@/types/prologProject";
import { useParams } from "react-router-dom";
import { BaseLayout } from "@/layouts/BaseLayout";
import { PrologProjectDetail } from "@/components/PrologProjectDetail";
import { getSingleProjectDetailAPI } from "@/constants/apiEndPoints";

const ShowSingleProject: FC = () => {
  const { projectId } = useParams<{ projectId: string }>();

  const { isLoading, data: project, error } = useFetch<IPrologProjectData>(getSingleProjectDetailAPI(projectId));

  if (isLoading) {
    return (
      <BaseLayout>
        <h4>Loading...</h4>
      </BaseLayout>
    );
  }

  if (error) {
    return (
      <BaseLayout>
        <h4>Error Message: {error.message}</h4>
      </BaseLayout>
    );
  }

  return <BaseLayout>{project ? <PrologProjectDetail {...project} /> : <h4>No Data Found</h4>}</BaseLayout>;
};

export default ShowSingleProject;
