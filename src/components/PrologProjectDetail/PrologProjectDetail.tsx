import { IPrologProjectData } from "@/types/prologProject";
import { FC } from "react";

const defaultColumns: { label: string; id: keyof IPrologProjectData }[] = [
  {
    label: "Name",
    id: "name",
  },
  {
    label: "Status",
    id: "status",
  },
  {
    label: "Language",
    id: "language",
  },
  {
    label: "Number of Issues",
    id: "numIssues",
  },
  {
    label: "Number of Events Last 24h",
    id: "numEvents24h",
  },
];

export const PrologProjectDetail: FC<IPrologProjectData> = (project) => {
  return (
    <div className="project-detail">
      <h2 className="mb-1">Project Detail: {project.id}</h2>
      <table>
        <tbody>
          {defaultColumns.map(({ label, id }) => (
            <tr key={id}>
              <td>
                <strong>{label}</strong>
              </td>
              <td> - </td>
              <td>{project[id]}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};
