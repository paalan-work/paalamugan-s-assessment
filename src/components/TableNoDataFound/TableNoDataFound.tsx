import { FC, ReactNode } from "react";

interface ITableNoDataFoundProps {
  children: ReactNode;
  colSpan?: number;
}

export const TableNoDataFound: FC<ITableNoDataFoundProps> = ({ children, colSpan = 1 }) => {
  return (
    <tr>
      <td colSpan={colSpan}>{children}</td>
    </tr>
  );
};
