import { FC } from "react";
import { Link } from "react-router-dom";

interface PrologProjectProps {
  id: string;
  name: string;
  status: string;
  language: string;
}

export const PrologProject: FC<PrologProjectProps> = ({ id, name, status, language }) => {
  return (
    <tr>
      <td>{name}</td>
      <td>{status}</td>
      <td>{language}</td>
      <td>
        <Link to={`/project/${id}`}>Show Project</Link>
      </td>
    </tr>
  );
};
