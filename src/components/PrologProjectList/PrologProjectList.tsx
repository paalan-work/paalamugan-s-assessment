import { FC } from "react";
import useFetch from "@/hooks/useFetch";
import { IPrologProjectData } from "@/types/prologProject";
import { TableNoDataFound } from "../TableNoDataFound/TableNoDataFound";
import { PrologProject } from "./PrologProject";
import commonStyles from "@/scss/common.module.scss";
import { getProjectListAPI } from "@/constants/apiEndPoints";

export const PrologProjectList: FC = () => {
  const { isLoading, data: projects, error } = useFetch<IPrologProjectData[]>(getProjectListAPI());

  if (isLoading) return <h4>Loading...</h4>;
  if (error) return <p>Error Message: {error.message}</p>;

  return (
    <table className="mt-1 min-width-40">
      <thead>
        <tr>
          <th>Name</th>
          <th>Status</th>
          <th>Language</th>
          <th>Link</th>
        </tr>
      </thead>
      <tbody>
        {!projects?.length && (
          <TableNoDataFound colSpan={4}>
            <p className={commonStyles["text-center"]}>No data found.</p>
          </TableNoDataFound>
        )}
        {projects &&
          projects.map((project) => (
            <PrologProject
              key={project.id}
              id={project.id}
              status={project.status}
              name={project.name}
              language={project.language}
            />
          ))}
      </tbody>
    </table>
  );
};
