import React from "react";
import { Link } from "react-router-dom";
import styles from "./header.module.scss";
import { useAuth } from "@/context/AuthContext";

const Header: React.FC = () => {
  const { isLoggedIn, logout, login } = useAuth();

  const onLogin = async () => {
    await login({ email: "johndoe@gmail.com", password: "test" });
    console.log("Successfully Logged In");
  };

  const onLogout = async () => {
    await logout();
    console.log("Successfully Logged Out");
  };

  return (
    <div className={styles["header-wrapper"]}>
      <div className={styles["left-header"]}>
        <h1>Header</h1>
        <Link to="/" className={styles["header-link"]}>
          Home
        </Link>
        <Link to="/about" className={styles["header-link"]}>
          About
        </Link>
      </div>
      <button type="button" onClick={isLoggedIn ? onLogout : onLogin}>
        {isLoggedIn ? "Logout" : "Login"}
      </button>
    </div>
  );
};

export default Header;
