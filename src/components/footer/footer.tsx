import React from "react";
import styles from "./footer.module.scss";
import { useAuth } from "@/context/AuthContext";

const Footer: React.FC = () => {
  const { isLoggedIn, logout } = useAuth();

  return (
    <footer>
      <div className={styles["footer-wrapper"]}>
        <div>Footer</div>
        {isLoggedIn && (
          <button type="button" onClick={logout}>
            Logout
          </button>
        )}
      </div>
    </footer>
  );
};

export default Footer;
