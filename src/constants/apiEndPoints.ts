const API_END_POINT = "https://prolog-api.profy.dev";

export const getProjectListAPI = () => {
  return `${API_END_POINT}/project`;
};

export const getSingleProjectDetailAPI = (projectId: string) => {
  return `${getProjectListAPI()}/${projectId}`;
};
