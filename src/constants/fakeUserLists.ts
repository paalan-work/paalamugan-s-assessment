import { IUserList } from "@/types/common";

export const fakeUserLists: IUserList[] = [
  {
    email: "johndoe@gmail.com",
    name: "John Doe",
    role: "admin",
    password: "test",
  },
];
