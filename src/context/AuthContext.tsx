import { fakeUserLists } from "@/constants/fakeUserLists";
import { ICredential, IUserList } from "@/types/common";
import React, { FC, ReactNode, createContext, useContext, useReducer } from "react";

export interface AuthState {
  user?: Omit<IUserList, "password"> | null;
  isLoggedIn: boolean;
}

interface AuthAction {
  type: "LOGIN" | "LOGOUT";
  payload?: AuthState["user"];
}

const initialState: AuthState = {
  user: null,
  isLoggedIn: false,
};

const authReducer = (state: AuthState, action: AuthAction) => {
  switch (action.type) {
    case "LOGIN":
      return { ...state, user: action.payload };
    case "LOGOUT":
      return { ...state, user: null };
    default:
      return state;
  }
};

export const AuthContext = createContext({
  ...initialState,
  login: async (credential: ICredential) => {},
  logout: async () => {},
});

interface AuthProviderProps {
  children: ReactNode;
}

export const AuthProvider: FC<AuthProviderProps> = ({ children }) => {
  const [auth, dispatch] = useReducer(authReducer, initialState);

  const login = async ({ email, password }: ICredential) => {
    const findUser = fakeUserLists.find((user) => user.email === email && user.password === password);

    if (!findUser) {
      throw new Error("Invalid email or password");
    }

    dispatch({ type: "LOGIN", payload: findUser });
  };

  const logout = async () => {
    dispatch({ type: "LOGOUT" });
  };

  const getLoggedIn = () => {
    return !!auth.user; // Incase, If we want we can check whether user logged in or not using expiry date here itself without modifying any of other code
  };

  return (
    <AuthContext.Provider value={{ user: auth.user, isLoggedIn: getLoggedIn(), login, logout }}>
      {children}
    </AuthContext.Provider>
  );
};

export const useAuth = () => {
  return useContext(AuthContext);
};
