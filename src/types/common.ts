export interface IUserList {
  email: string;
  name: string;
  role: string;
  password: string;
}

export interface ICredential {
  email: string;
  password: string;
}
