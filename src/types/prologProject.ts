export interface IPrologProjectData {
  id: string;
  numIssues: number;
  numEvents24h: number;
  status: string;
  name: string;
  language: string;
}
