import { FC, PropsWithChildren } from "react";
import commonStyles from "@/scss/common.module.scss";

export const BaseLayout: FC<PropsWithChildren> = ({ children }) => {
  return <div className={`${commonStyles["text-center"]} ${commonStyles["container"]}`}>{children}</div>;
};
